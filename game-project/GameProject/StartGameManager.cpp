#include "GameCore.h"
#include "StartGameManager.h"
#include "FileSystem.h"

IMPLEMENT_DYNAMIC_CLASS(StartGameManager)

void StartGameManager::Update()
{
	// Check for s key to start
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::S)) {
		GameEngine::Instance().LoadNewScene(levelName);
	}

	// Check for l key to load game
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::L)) {
		json::JSON document = FileSystem::Instance().Load("../Assets/Saves/SaveFile.json");
		if (document.IsNull()) {
			return;
		}

		GameEngine::Instance().LoadNewSceneJSON(document);
	}

	// Check for q key to quit
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Q)) {
		RenderSystem::Instance().CloseWindow();
	}
}

json::JSON StartGameManager::Save()
{
	json::JSON document;
	document["className"] = "StartGameManager";
	document["uid"] = this->GetGUID();

	return document;
}

void StartGameManager::Load(json::JSON& json_game_object)
{
	ASSERT(json_game_object.hasKey("startLevel"), "StartManager missing 'startLevel'");
	levelName = json_game_object["startLevel"].ToString();
}
