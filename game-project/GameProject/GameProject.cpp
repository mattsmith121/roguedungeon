///-------------------------------------------------------------------------------------------------
// file: GameOBject.cpp
//
// author: William Barry
// date: 2020
//
// summary:	Entry point for the program
///-------------------------------------------------------------------------------------------------
#include "GameCore.h"
#include "TileManager.h"

extern void GameRegisterClasses();

int main()
{
	GameRegisterClasses();

	GameEngine::Instance().Initialize();


	GameEngine::Instance().Update();
}
