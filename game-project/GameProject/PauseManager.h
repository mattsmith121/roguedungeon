#pragma once
#ifndef _PAUSEMANAGER_H_
#define _PAUSEMANAGER_H_

#include "Component.h"

class PauseManager : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(PauseManager, Component)

protected:
	void Update() override;

	/// <summary>
	/// Save the pausemanager data to json
	/// </summary>
	/// <returns>JSON containing pausemanager data</returns>
	json::JSON Save() override;
};

#endif // !_PAUSEMANAGER_H_