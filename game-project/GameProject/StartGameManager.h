#pragma once
#ifndef _STARTGAMEMANAGER_H_
#define _STARTGAMEMANAGER_H_

#include "Component.h"

class StartGameManager : public Component
{

	DECLARE_DYNAMIC_DERIVED_CLASS(StartGameManager, Component)

private:
	std::string levelName;

protected:
	void Update() override;

	/// <summary>
	/// Save the start game manager data to json
	/// </summary>
	/// <returns>JSON containing start game manager data</returns>
	json::JSON Save() override;

	/// <summary>
	/// Load start game manager data from json
	/// </summary>
	/// <param name="json_game_object">JSON containing the data</param>
	void Load(json::JSON& json_game_object) override;
};

#endif // !_STARTGAMEMANAGER_H_