#include "GameCore.h"
#include "PauseManager.h"

IMPLEMENT_DYNAMIC_CLASS(PauseManager)

void PauseManager::Update() {
	// Check for c to continue
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::C)) {
		std::list<GameObject*> pauseObjects = GameObjectManager::Instance().GetGameObjectsWithComponent("PauseObject");
		for (auto it : pauseObjects) {
			GameObjectManager::Instance().RemoveGameObject(it);
		}
	}

	// Check for s key to save game
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::S)) {
		GameObjectManager::Instance().Save();
	}

	// Check for q key to quit
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Q)) {
		RenderSystem::Instance().CloseWindow();
	}
}

json::JSON PauseManager::Save()
{
	json::JSON document;
	document["className"] = "PauseManager";
	document["uid"] = this->GetGUID();

	return document;
}
