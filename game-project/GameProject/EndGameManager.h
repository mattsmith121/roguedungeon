#pragma once
#ifndef _ENDGAMEMANAGER_H_
#define _ENDGAMEMANAGER_H_

#include "Component.h"

class EndGameManager : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(EndGameManager, Component)

protected:
	void Update() override;

	/// <summary>
	/// Save the end game manager data to json
	/// </summary>
	/// <returns>JSON containing end game manager data</returns>
	json::JSON Save() override;
};

#endif // !_ENDGAMEMANAGER_H_