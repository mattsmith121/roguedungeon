#include "GameCore.h"
#include "ExitDoor.h"
#include "Tile.h"
#include "GameManager.h"

IMPLEMENT_DYNAMIC_CLASS(ExitDoor)

void ExitDoor::Initialize() {
	Component::Initialize();

	// Get gamemanager
	GameObject* gameManagerObj = GameObjectManager::Instance().GetGameObject("GameManager");
	if (gameManagerObj == nullptr) {
		// TODO: handle no game manager error
		return;
	}
	GameManager* gameManager = (GameManager*)(gameManagerObj->GetComponent("GameManager"));

	// Attach to tile no matter what
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	t->AttachObject(game_object);
}

json::JSON ExitDoor::Save()
{
	json::JSON document;
	document["className"] = "ExitDoor";
	document["uid"] = this->GetGUID();

	return document;
}
