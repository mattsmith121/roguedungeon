#include "GameCore.h"
#include "Ghost.h"
#include "Tile.h"
#include "GameManager.h"
#include "Enemy.h"
#include "Player.h"
#include "Factory.h"

IMPLEMENT_DYNAMIC_CLASS(Ghost)

void Ghost::Initialize() {
	Component::Initialize();

	// Get player game object
	player = GameObjectManager::Instance().GetGameObject("Player");

	// Get gamemanager
	GameObject* gameManagerObj = GameObjectManager::Instance().GetGameObject("GameManager");
	if (gameManagerObj == nullptr) {
		// TODO: handle no game manager error
		return;
	}
	gameManager = (GameManager*)(gameManagerObj->GetComponent("GameManager"));

	// Attach to tile if not already occupied
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	if (!t->isOccupied()) {
		t->AttachObject(game_object);
	}
}

void Ghost::Update()
{
	if (dead) {
		return;
	}

	// Get enemy component
	Enemy* enemy = (Enemy*)game_object->GetComponent("Enemy");
	if (enemy == nullptr) {
		return;
	}

	if (enemy->isActive()) {
		if (enemy->GetHealth() <= 0){
			Die();
			return;
		}
		sf::Vector2f playerPos = player->GetPosition();
		sf::Vector2f pos = game_object->GetPosition();

		MoveOrAttack(pos, playerPos);

		// De-activate after done turn
		enemy->Deactivate();
	}
}

void Ghost::Load(json::JSON& jsonComponent)
{
	Component::Load(jsonComponent);

	ASSERT(jsonComponent.hasKey("attack"), "Ghost missing 'attack' field");
	attackDamage = jsonComponent["attack"].ToInt();

	// Poison ghost (optional)
	if (jsonComponent.hasKey("poison")) {
		poison = jsonComponent["poison"].ToBool();
	}
}

json::JSON Ghost::Save()
{
	json::JSON document;
	document["className"] = "Ghost";
	document["uid"] = this->GetGUID();

	document["attack"] = attackDamage;
	document["poison"] = poison;

	return document;
}

void Ghost::AssignFactory(Factory* factory, int id)
{
	this->factory = factory;
	this->factoryID = id;
}

void Ghost::MoveOrAttack(sf::Vector2f pos, sf::Vector2f playerPos)
{
	// Move left/right has priority
	// Then try to move up/down

	Tile *curTile = gameManager->GetTile(pos);

	bool close = (abs(abs(playerPos.x) - abs(pos.x)) == 1 && abs(playerPos.y) - abs(pos.y) == 0 ||
		abs(abs(playerPos.y) - abs(pos.y)) == 1 && abs(playerPos.x) - abs(pos.x) == 0);

	// Move left
	if (playerPos.x < pos.x) {
		sf::Vector2f newPos = pos + sf::Vector2f(-1.0f, 0);
		if (!curTile->GetWall(3) && !gameManager->GetTile(newPos)->GetWall(2)) {
			if (close) {
				Attack();
				return;
			}
			
			if (MoveToTile(newPos, curTile)) {
				return;
			}
		}
	}
	
	// Move right
	if (playerPos.x > pos.x) {
		sf::Vector2f newPos = pos + sf::Vector2f(1.0f, 0);
		if (!curTile->GetWall(2) && !gameManager->GetTile(newPos)->GetWall(3)) {
			if (close) {
				Attack();
				return;
			}

			if (MoveToTile(newPos, curTile)) {
				return;
			}
		}
	}
	
	// Move up
	if (playerPos.y < pos.y) {
		sf::Vector2f newPos = pos + sf::Vector2f(0, -1.0f);
		if (!curTile->GetWall(0) && !gameManager->GetTile(newPos)->GetWall(1)) {
			if (close) {
				Attack();
				return;
			}

			if (MoveToTile(newPos, curTile)) {
				return;
			}
		}
	}
	
	// Move down
	if (playerPos.y > pos.y) {
		sf::Vector2f newPos = pos + sf::Vector2f(0, 1.0f);
		if (!curTile->GetWall(1) && !gameManager->GetTile(newPos)->GetWall(0)) {
			if (close) {
				Attack();
				return;
			}

			if (MoveToTile(newPos, curTile)) {
				return;
			}
		}
	}
}

void Ghost::Attack()
{
	// Hit player
	((Player*)player->GetComponent("Player"))->TakeDamage(attackDamage, poison);
}

bool Ghost::MoveToTile(sf::Vector2f movePos, Tile* curTile)
{
	Tile* moveTile = gameManager->GetTile(movePos);
	if (!moveTile->isOccupied()) {
		// Attach to new tile
		moveTile->AttachObject(game_object);
		// Unattach from old tile
		curTile->UnattachObject();
		// Update position
		game_object->SetPosition(movePos);
		return true;
	}
	return false;
}

void Ghost::Die()
{
	// Stop updating
	dead = true;

	// Free spot in object pool
	factory->FreeEnemy(factoryID);

	// Unattach from tile
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	t->UnattachObject();

	game_object->RemoveAllComponents();
}
