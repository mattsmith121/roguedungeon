#include "GameCore.h"
#include "Player.h"
#include "GameManager.h"
#include "Tile.h"
#include "Enemy.h"
#include "Potion.h"
#include "Sprite.h"
#include "TextSprite.h"

IMPLEMENT_DYNAMIC_CLASS(Player)

void Player::Initialize() {
	Component::Initialize();

	// Get game manager
	GameObject* gameManagerObj = GameObjectManager::Instance().GetGameObject("GameManager");
	if (gameManagerObj == nullptr) {
		// TODO: handle no game manager error
		return;
	}
	this->gameManager = (GameManager*)(gameManagerObj->GetComponent("GameManager"));

	// Attach to tile
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	t->AttachObject(game_object);
}

void Player::Update()
{
	if (!gameManager->isPlayerTurn() || gameManager->isGamePaused()) {
		return;
	}

	// TODO: Delete me
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::D)) {
		gameManager->PlayerDied();
		return;
	}

	Tile* curTile = gameManager->GetTile(game_object->GetPosition());
	sf::Vector2f movePos;

	// Pick up item if there
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::E)) {
		// Is there an item on our current tile
		if (curTile->hasItem()) {
			int p = curTile->GetItem()->GetPotionType();
			if (p == 0) {
				healthPotions++;
				UpdateUI("HealthPotionText", "Health Potions: " + std::to_string(healthPotions));
			}
			else {
				antidotes++;
				UpdateUI("AntidoteText", "Antidotes: " + std::to_string(antidotes));
			}
			curTile->UnattachItem();
			EndTurn();
			return;
		}
	}

	// Use health potion
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::H) && healthPotions > 0) {
		healthPotions--;
		health += 50;
		if (health > 100) health = 100;
		UpdateUI("HealthText", "Health: " + std::to_string(health));
		UpdateUI("HealthPotionText", "Health Potions: " + std::to_string(healthPotions));
		EndTurn();
		return;
	}

	// Use antidote
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::A) && antidotes > 0 && poisoned) {
		antidotes--;
		poisoned = false;
		UpdateUI("AntidoteText", "Antidotes: " + std::to_string(antidotes));
		((Sprite*)game_object->GetComponent("Sprite"))->SetColor(sf::Color::White);
		EndTurn();
		return;
	}

#pragma region Movement
	// Is control down?
	bool ctrlDown = InputManager::Instance().KeyboardKeyDown(sf::Keyboard::LControl) || 
		InputManager::Instance().KeyboardKeyDown(sf::Keyboard::RControl);

	// Are we trying to move or attack and in which direction?
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Left)){
		sf::Vector2f newPos = game_object->GetPosition() + sf::Vector2f(-1.0f, 0);
		// Don't go off map
		if (newPos.x < 0 || newPos.y < 0 || newPos.x >= 10 || newPos.y >= 10)
			return;
		// Check if walls are in the way
		if (!curTile->GetWall(3) && !gameManager->GetTile(newPos)->GetWall(2)) {
			if (ctrlDown) {
				if (TryAttack(newPos)) {
					// Turn over
					EndTurn();
				}
				return;
			}
			else {
				movePos = newPos;
			}
		}
		else {
			return;
		}
	} else if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Right)) {
		sf::Vector2f newPos = game_object->GetPosition() + sf::Vector2f(1.0f, 0);
		// Don't go off map
		if (newPos.x < 0 || newPos.y < 0 || newPos.x >= 10 || newPos.y >= 10)
			return;
		// Check if walls are in the way
		if (!curTile->GetWall(2) && !gameManager->GetTile(newPos)->GetWall(3)) {
			if (ctrlDown) {
				if (TryAttack(newPos)) {
					// Turn over
					EndTurn();
				}
				return;
			}
			else {
				movePos = newPos;
			}
		}
		else {
			return;
		}
	} else if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Up) && !curTile->GetWall(0)) {
		sf::Vector2f newPos = game_object->GetPosition() + sf::Vector2f(0, -1.0f);
		// Don't go off map
		if (newPos.x < 0 || newPos.y < 0 || newPos.x >= 10 || newPos.y >= 10)
			return;
		// Check if walls are in the way
		if (!curTile->GetWall(0) && !gameManager->GetTile(newPos)->GetWall(1)) {
			if (ctrlDown) {
				if (TryAttack(newPos)) {
					// Turn over
					EndTurn();
				}
				return;
			}
			else {
				movePos = newPos;
			}
		}
		else {
			return;
		}
	} else if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Down) && !curTile->GetWall(1)) {
		sf::Vector2f newPos = game_object->GetPosition() + sf::Vector2f(0, 1.0f);
		// Don't go off map
		if (newPos.x < 0 || newPos.y < 0 || newPos.x >= 10 || newPos.y >= 10)
			return;
		// Check if walls are in the way
		if (!curTile->GetWall(1) && !gameManager->GetTile(newPos)->GetWall(0)) {
			if (ctrlDown) {
				if (TryAttack(newPos)) {
					// Turn over
					EndTurn();
				}
				return;
			}
			else {
				movePos = newPos;
			}
		}
		else {
			return;
		}
	} else {
		return;
	}

	Tile* moveTile = gameManager->GetTile(movePos);
	if (!moveTile->isOccupied()) {
		// Attach to new tile
		moveTile->AttachObject(game_object);
		// Unattach from old tile
		curTile->UnattachObject();
		// Update position
		game_object->SetPosition(movePos);
		// Turn over
		EndTurn();
	}
	// But, is it occupied by the exit door?
	else if (moveTile->GetAttachedObject() != nullptr &&
		moveTile->GetAttachedObject()->GetComponent("ExitDoor") != nullptr) {
		gameManager->PlayerWin();
	}
#pragma endregion Movement
}

bool Player::TryAttack(sf::Vector2f pos)
{
	Tile* aTile = gameManager->GetTile(pos);
	if (aTile == nullptr) {
		return false;
	}

	GameObject* attachedObject = aTile->GetAttachedObject();
	if (attachedObject != nullptr) {
		Enemy* enemy = (Enemy*)(attachedObject->GetComponent("Enemy"));
		if (enemy == nullptr) {
			return false;
		}

		enemy->TakeDamage(10);
		return true;
	}
}

void Player::EndTurn()
{
	if (poisoned) {
		health -= 5;
		UpdateUI("HealthText", "Health: " + std::to_string(health));
	}

	if (health <= 0) {
		// Tell game manager player died
		gameManager->PlayerDied();
	}
	else {
		gameManager->EndPlayerTurn();
	}
}

void Player::TakeDamage(int damage, bool poison)
{
	health -= damage;
	UpdateUI("HealthText", "Health: " + std::to_string(health));
	if (poison) {
		poisoned = true;
		((Sprite*)game_object->GetComponent("Sprite"))->SetColor(sf::Color::Green);
	}
	if (health <= 0) {
		// Tell game manager player died
		gameManager->PlayerDied();
	}
}

void Player::Load(json::JSON& json_component)
{
	Component::Load(json_component);

	ASSERT(json_component.hasKey("health"), "Player health missing");
	health = json_component["health"].ToInt();
}


json::JSON Player::Save()
{
	json::JSON document;
	document["className"] = "Player";
	document["uid"] = this->GetGUID();
	document["health"] = health;

	return document;
}

void Player::UpdateUI(std::string uiName, std::string text)
{
	GameObject* go = GameObjectManager::Instance().GetGameObject(uiName);
	if (go != nullptr) {
		((TextSprite*)go->GetComponent("TextSprite"))->SetText(text);
	}
}


