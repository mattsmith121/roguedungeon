#pragma once
#ifndef _POTION_H_
#define _POTION_H_

#include "Component.h"

class Potion : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Potion, Component)

private:
	/// <summary>
	/// health: 0, antidote: 1
	/// </summary>
	int potionType = 0;

public:
	int GetPotionType();

protected:
	void Initialize() override;

	/// <summary>
	/// Load potion data from json
	/// </summary>
	/// <param name="jsonComponent">JSON containing potion data</param>
	void Load(json::JSON& jsonComponent);

	/// <summary>
	/// Save potion data to json
	/// </summary>
	/// <returns>JSON containging potion data</returns>
	json::JSON Save() override;
};

#endif // !_POTION_H_