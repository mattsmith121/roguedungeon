#include "GameCore.h"
#include "TileData.h"

TileData::TileData(json::JSON& jsonObj)
{
	// Load in id, default to -1
	if (jsonObj.hasKey("ID")) {
		id = jsonObj["ID"].ToInt();
	}
	else {
		std::cerr << "Tile missing id" << std::endl;
		id = -1;
	}

	// Load in texture
	if (jsonObj.hasKey("Texture")) {
		texture = jsonObj["Texture"].ToString();
	}
	else {
		texture = "defaultTile.png";
	}

	// Load wall data
	if (jsonObj.hasKey("WallTop")) {
		wallTop = jsonObj["WallTop"].ToBool();
	}
	else {
		wallTop = false;
	}

	if (jsonObj.hasKey("WallBottom")) {
		wallBottom = jsonObj["WallBottom"].ToBool();
	}
	else {
		wallBottom = false;
	}

	if (jsonObj.hasKey("WallRight")) {
		wallRight = jsonObj["WallRight"].ToBool();
	}
	else {
		wallRight = false;
	}

	if (jsonObj.hasKey("WallLeft")) {
		wallLeft = jsonObj["WallLeft"].ToBool();
	}
	else {
		wallLeft = false;
	}
}

TileData::TileData(std::string texture)
{
	this->texture = texture;
	wallTop = wallBottom = wallRight = wallLeft = false;
}

std::string& TileData::GetTexture()
{
	return texture;
}

bool TileData::GetWall(int direction)
{
	switch (direction) {
	case 0:
		return wallTop;
	case 1:
		return wallBottom;
	case 2:
		return wallRight;
	case 3:
		return wallLeft;
	default:
		return false;
	}
}

int TileData::GetID() {
	return id;
}
