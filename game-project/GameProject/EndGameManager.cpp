#include "GameCore.h"
#include "EndGameManager.h"

IMPLEMENT_DYNAMIC_CLASS(EndGameManager)

void EndGameManager::Update()
{
	// Check for space key press back to menu
	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Space)) {
		GameEngine::Instance().LoadNewScene("../Assets/StartScene.json");
	}
}

json::JSON EndGameManager::Save()
{
	json::JSON document;
	document["className"] = "EndGameManager";
	document["uid"] = this->GetGUID();

	return document;
}
