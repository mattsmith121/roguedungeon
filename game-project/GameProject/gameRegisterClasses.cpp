///-------------------------------------------------------------------------------------------------
// file: gameRegisterClasses.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "GameCore.h"

// Game Include
#include "Tile.h"
#include "GameManager.h"
#include "Player.h"
#include "Factory.h"
#include "Enemy.h"
#include "Ghost.h"
#include "EndGameManager.h"
#include "ExitDoor.h"
#include "Potion.h"
#include "StartGameManager.h"
#include "PauseManager.h"
#include "PauseObject.h"

/// <summary>
/// Register game project classes for CRtti
/// </summary>
void GameRegisterClasses()
{
	REGISTER_DYNAMIC_CLASS(Tile);
	REGISTER_DYNAMIC_CLASS(GameManager);
	REGISTER_DYNAMIC_CLASS(Player);
	REGISTER_DYNAMIC_CLASS(Factory);
	REGISTER_DYNAMIC_CLASS(Ghost);
	REGISTER_DYNAMIC_CLASS(Enemy);
	REGISTER_DYNAMIC_CLASS(EndGameManager);
	REGISTER_DYNAMIC_CLASS(ExitDoor);
	REGISTER_DYNAMIC_CLASS(Potion);
	REGISTER_DYNAMIC_CLASS(StartGameManager);
	REGISTER_DYNAMIC_CLASS(PauseManager);
	REGISTER_DYNAMIC_CLASS(PauseObject);
}