#pragma once
#ifndef _EXITDOOR_H_
#define _EXITDOOR_H_

#include "Component.h"

class ExitDoor : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(ExitDoor, Component)

protected:
	void Initialize() override;
	
	/// <summary>
	/// Save exit door to json
	/// </summary>
	/// <returns>JSON containing exit door data</returns>
	json::JSON Save() override;
};

#endif // !_EXITDOOR_H_