#pragma once
#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Component.h"

class GameManager;

class Enemy :  public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Enemy, Component)

private:
	GameManager* gameManager;
	bool activated;
	int health;

public:
	/// <summary>
	/// Activate the enemy
	/// </summary>
	void Activate();

	/// <summary>
	/// Deactivate the enemy
	/// </summary>
	void Deactivate();

	/// <summary>
	/// Is the enemy active?
	/// </summary>
	/// <returns>True if enemy active, false otherwise</returns>
	bool isActive();

	/// <summary>
	/// Deal damage to the enemy
	/// </summary>
	/// <param name="damage">How much damage to deal.</param>
	void TakeDamage(int damage);

	/// <summary>
	/// Get current health
	/// </summary>
	/// <returns>Health</returns>
	int GetHealth();

protected:
	void Initialize() override;

	/// <summary>
	/// Load enemy from JSON
	/// </summary>
	/// <param name="jsonComponent">JSON info on enemy</param>
	void Load(json::JSON& jsonComponent) override;

	/// <summary>
	/// Save enemy info to json
	/// </summary>
	/// <returns>JSON containing enemy info</returns>
	json::JSON Save() override;
};

#endif // !_ENEMY_H_