#pragma once

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Component.h"

class GameManager;

class Player : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Player, Component)

private:
	int health = 100;
	int healthPotions = 0;
	int antidotes = 0;
	bool poisoned = false;
	GameManager* gameManager;
	bool TryAttack(sf::Vector2f pos);
	void EndTurn();
	void UpdateUI(std::string uiName, std::string text);

public:
	void TakeDamage(int damage, bool poison = false);

	/// <summary>
	/// Load player data from json
	/// </summary>
	/// <param name="json_component">JSON containing player data</param>
	void Load(json::JSON& json_component) override;

	/// <summary>
	/// Save player data to json
	/// </summary>
	/// <returns>JSON containing player data</returns>
	json::JSON Save() override;

protected:
	void Update() override;
	void Initialize() override;
};

#endif // !_PLAYER_H_