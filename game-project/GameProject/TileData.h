#pragma once
#ifndef _TILEDATA_H_
#define _TILEDATA_H_

#include "GameCore.h"

class TileData
{
	int id;
	std::string texture;
	bool wallTop, wallBottom, wallRight, wallLeft;

public:
	TileData(json::JSON& jsonObj);
	TileData(std::string texture);

	/// <summary>
	/// Gets the tile's texture
	/// </summary>
	/// <returns>Name of the texture</returns>
	std::string& GetTexture();

	/// <summary>
	/// Is there a wall in the given direction
	/// </summary>
	/// <param name="direction">0 = top, 1 = bottom, 2 = right, 3 = left</param>
	/// <returns>True if wall, false otherwise</returns>
	bool GetWall(int direction);

	/// <summary>
	/// Gets the unique tile id
	/// </summary>
	/// <returns>Tile id</returns>
	int GetID();
};

#endif // !_TILEDATA_H_