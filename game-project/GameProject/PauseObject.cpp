#include "GameCore.h"
#include "PauseObject.h"

IMPLEMENT_DYNAMIC_CLASS(PauseObject)

json::JSON PauseObject::Save() {
	json::JSON document;
	document["className"] = "PauseObject";
	document["uid"] = this->GetGUID();

	return document;
}