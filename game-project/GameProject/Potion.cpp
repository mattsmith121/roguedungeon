#include "GameCore.h"
#include "Potion.h"
#include "GameManager.h"
#include "Tile.h"

IMPLEMENT_DYNAMIC_CLASS(Potion)

int Potion::GetPotionType()
{
	return potionType;
}

void Potion::Initialize()
{
	Component::Initialize();

	// Get game manager
	GameObject* gameManagerObj = GameObjectManager::Instance().GetGameObject("GameManager");
	if (gameManagerObj == nullptr) {
		// TODO: handle no game manager error
		return;
	}
	GameManager* gameManager = (GameManager*)(gameManagerObj->GetComponent("GameManager"));

	// Attach to tile no matter what
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	t->AttachItem(this);
}

void Potion::Load(json::JSON& jsonComponent)
{
	Component::Load(jsonComponent);

	if (jsonComponent.hasKey("type")) {
		int type = jsonComponent["type"].ToInt();
		if (type != 0 && type != 1) {
			potionType = 0;
		}
		else {
			potionType = type;
		}
	}
	else {
		potionType = 0;
	}
}

json::JSON Potion::Save()
{
	json::JSON document;
	document["className"] = "Potion";
	document["uid"] = this->GetGUID();
	document["type"] = potionType;

	return document;
}
