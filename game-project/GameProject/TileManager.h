#pragma once
#ifndef _TILEMANAGER_H_
#define _TILEMANAGER_H_

class TileData;

class TileManager
{
	std::vector<TileData*> tileData;

public:
	void LoadTileData();

	/// <summary>
	/// Creates a tile using tile data specified by id
	/// </summary>
	/// <param name="id">ID of the tile</param>
	/// <param name="position">Position of the tile</param>
	/// <returns>GameObject with tile and sprite components or nullptr if no matching id found</returns>
	GameObject* CreateTile(int id, sf::Vector2f position);
};

#endif // !_TILEMANAGER_H_