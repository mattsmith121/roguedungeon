#include "GameCore.h"
#include "GameManager.h"
#include "TileManager.h"
#include "Tile.h"
#include "Player.h"
#include "Factory.h"
#include "Enemy.h"
#include "FileSystem.h"

IMPLEMENT_DYNAMIC_CLASS(GameManager)

void GameManager::Load(json::JSON& jsonComponent){
	Component::Load(jsonComponent);
	
	// Load in first level
	ASSERT(jsonComponent.hasKey("DefaultLevel"), "GameManager missing 'DefaultLevel'");
	defaultLevel = jsonComponent["DefaultLevel"].ToString();
}

json::JSON GameManager::Save()
{
	json::JSON document;
	document["className"] = "GameManager";
	document["uid"] = this->GetGUID();
	document["DefaultLevel"] = defaultLevel;

	return document;
}

void GameManager::Update() {
	Component::Update();

	if (InputManager::Instance().KeyboardKeyPressed(sf::Keyboard::Tilde)) {
		paused = true;
		json::JSON pauseDocument = FileSystem::Instance().Load("../Assets/PauseScreen.json");
		GameObjectManager::Instance().Load(pauseDocument);
	}

	if (turnState == TurnState::Enemy) {
		// Get all enemies
		std::list<GameObject*> enemies = GameObjectManager::Instance().GetGameObjectsWithComponent("Enemy");
		bool allDone = true;
		for (auto it : enemies) {
			if (((Enemy*)(it->GetComponent("Enemy")))->isActive()) {
				allDone = false;
				break;
			}
		}

		if (allDone) {
			turnState = TurnState::Player;
		}
	}
}

void GameManager::TransitionToLevel()
{
	// Load in level file json
	json::JSON levelDocument = FileSystem::Instance().Load(defaultLevel);

	// Create tilemap from level file
	ASSERT(levelDocument.hasKey("Tilemap"), "Level missing 'Tilemap'");
	json::JSON tilemap = levelDocument["Tilemap"];

	levelMap = std::vector<std::vector<GameObject*>>(10);
	for (int x = 0; x < tilemap.size(); x++) {
		levelMap[x] = std::vector<GameObject*>(10);
		for (int y = 0; y < tilemap[x].size(); y++) {
			levelMap[x][y] = tileManager->CreateTile(tilemap[y][x].ToInt(), sf::Vector2f(x, y));
		}
	}
}

GameManager::~GameManager()
{
	// Need to clear tilemap array
	// GameObjects are cleared automatically by GameObjectManager
	// but need to clear pointers
	for (int i = 0; i < levelMap.size(); i++) {
		levelMap[i].clear();
	}

	levelMap.clear();


}

void GameManager::Initialize() {
	Component::Initialize();

	// Create tile manager and load tile data from file
	tileManager = new TileManager();
	tileManager->LoadTileData();

	TransitionToLevel();
}

Tile* GameManager::GetTile(sf::Vector2f position) {
	int x = (int)position.x;
	int y = (int)position.y;
	if (levelMap.size() == 0) {
		return nullptr;
	}

	if (levelMap.size() <= x){
		return nullptr;
	}
	else {
		if (levelMap[x].size() <= y) {
			return nullptr;
		}
	}

	return (Tile*)(levelMap[x][y]->GetComponent("Tile"));
}

void GameManager::EndPlayerTurn() {
	// Change turn state
	turnState = TurnState::Enemy;

	// Activate all enemies
	std::list<GameObject*> enemies = GameObjectManager::Instance().GetGameObjectsWithComponent("Enemy");
	for (auto it : enemies) {
		((Enemy*)(it->GetComponent("Enemy")))->Activate();
	}
}

bool GameManager::isPlayerTurn() {
	return turnState == TurnState::Player;
}

void GameManager::PlayerDied()
{
	GameEngine::Instance().LoadNewScene("../Assets/LoseScene.json");
}

void GameManager::PlayerWin() {
	GameEngine::Instance().LoadNewScene("../Assets/WinScene.json");
}

bool GameManager::isGamePaused()
{
	return paused;
}
