#include "GameCore.h"
#include "Tile.h"
#include "Potion.h"

IMPLEMENT_DYNAMIC_CLASS(Tile)

void Tile::Load(json::JSON& jsonComponent)
{
	if (jsonComponent.hasKey("WallTop")) {
		wallTop = jsonComponent["WallTop"].ToBool();
	}
	else {
		wallTop = false;
	}

	if (jsonComponent.hasKey("WallBottom")) {
		wallBottom = jsonComponent["WallBottom"].ToBool();
	}
	else {
		wallBottom = false;
	}

	if (jsonComponent.hasKey("WallRight")) {
		wallRight = jsonComponent["WallRight"].ToBool();
	}
	else {
		wallRight = false;
	}

	if (jsonComponent.hasKey("WallLeft")) {
		wallLeft = jsonComponent["WallLeft"].ToBool();
	}
	else {
		wallLeft = false;
	}
}

json::JSON Tile::Save()
{
	// Never save tile data, always loaded by gameManager through tilemap
	return json::JSON();
}

void Tile::SetData(bool wallTop, bool wallBottom, bool wallRight, bool wallLeft)
{
	this->wallTop = wallTop;
	this->wallBottom = wallBottom;
	this->wallRight = wallRight;
	this->wallLeft = wallLeft;
}

bool Tile::GetWall(int direction)
{
	switch (direction) {
	case 0:
		return wallTop;
	case 1:
		return wallBottom;
	case 2:
		return wallRight;
	case 3:
		return wallLeft;
	default:
		return false;
	}
}

void Tile::UnattachObject()
{
	attachedObject = nullptr;
}

void Tile::AttachObject(GameObject* gameObject)
{
	attachedObject = gameObject;
}

void Tile::AttachItem(Potion* potion)
{
	attachedItem = potion;
}

void Tile::UnattachItem()
{
	// Once item is unattached, it is removed from the game
	GameObjectManager::Instance().RemoveGameObject(attachedItem->GetGameObject());
	attachedItem = nullptr;
}

bool Tile::hasItem()
{
	return attachedItem != nullptr;
}

Potion* Tile::GetItem()
{
	return attachedItem;
}

bool Tile::isOccupied()
{
	return attachedObject != nullptr;
}

GameObject* Tile::GetAttachedObject()
{
	return attachedObject;
}
