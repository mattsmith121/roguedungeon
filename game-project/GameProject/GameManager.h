#pragma once
#ifndef _GAMEMANAGER_H_
#define _GAMEMANAGER_H_

#include "Component.h"

class TileManager;
class Tile;

class GameManager : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(GameManager, Component)

	enum class TurnState {
		Player,
		Enemy
	};

	TileManager* tileManager;
	std::vector<std::vector<GameObject*>> levelMap;
	std::string defaultLevel;
	GameObject* mainMenu;
	GameObject* player;
	GameObject* factory;
	TurnState turnState;
	bool paused;

	void TransitionToLevel();

public:
	~GameManager();

	/// <summary>
	/// Get tile from the tilemap
	/// </summary>
	/// <param name="x">X coordinate</param>
	/// <param name="y">Y coordinate</param>
	/// <returns>Tile at position x,y. Nullptr if bad coords or no tilemap active.</returns>
	Tile* GetTile(sf::Vector2f position);

	/// <summary>
	/// Player has finished their turn
	/// </summary>
	void EndPlayerTurn();

	/// <summary>
	/// Is it the player's turn?
	/// </summary>
	/// <returns>True if it is the player's turn, false otherwise</returns>
	bool isPlayerTurn();

	/// <summary>
	/// Signal the game manager that the player has died and the game is over.
	/// </summary>
	void PlayerDied();

	/// <summary>
	/// Signal the game manager that the player has won and the game is over.
	/// </summary>
	void PlayerWin();

	/// <summary>
	/// Is the game currently paused?
	/// </summary>
	/// <returns>True if the game is paused, alse otherwise</returns>
	bool isGamePaused();

protected:

	/// <summary>
	/// Initialize the game manager
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// Load game manager data
	/// </summary>
	/// <param name="json_component">json data</param>
	void Load(json::JSON& jsonComponent) override;

	/// <summary>
	/// Save game manager data to json
	/// </summary>
	/// <returns>JSON containing game manager data</returns>
	json::JSON Save() override;

	/// <summary>
	/// Updates the game manager
	/// </summary>
	void Update() override;
};

#endif // !_GAMEMANAGER_H_