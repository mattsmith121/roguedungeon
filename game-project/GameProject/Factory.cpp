#include "GameCore.h"
#include "Factory.h"
#include "GameManager.h"
#include "Tile.h"
#include "FileSystem.h"

#include <chrono>
#include <stdlib.h>

IMPLEMENT_DYNAMIC_CLASS(Factory)

void Factory::Initialize() {
	Component::Initialize();

	// Get gamemanager
	GameObject* gameManagerObj = GameObjectManager::Instance().GetGameObject("GameManager");
	if (gameManagerObj == nullptr) {
		// TODO: handle no game manager error
		return;
	}
	gameManager = (GameManager*)(gameManagerObj->GetComponent("GameManager"));

	// Attach to tile if not already occupied
	if (!dead) {
		Tile* t = gameManager->GetTile(game_object->GetPosition());
		if (!t->isOccupied()) {
			t->AttachObject(game_object);
		}
	}

	// If we are dead, remove sprite component
	if (dead) {
		game_object->RemoveComponentByName("Sprite");
	}
}

void Factory::Load(json::JSON& json_component)
{
	Component::Load(json_component);

	// Spawn timer is optional
	if (json_component.hasKey("spawnTimer")) {
		spawnTimer = json_component["spawnTimer"].ToInt();
	}

	// Spawn Rate is optional
	if (json_component.hasKey("spawnRate")) {
		spawnRate = json_component["spawnRate"].ToInt();
	}

	// Death is optional
	if (json_component.hasKey("dead")) {
		dead = json_component["dead"].ToBool();
	}

	// Initialize ghost pool
	// Do this here since might initialize enemies
	for (int i = 0; i < 12; i++) {
		ghostTracker[i] = true;
	}

	// List of enemies is optional
	if (json_component.hasKey("enemies")) {
		json::JSON enemyList = json_component["enemies"];
		for (int i = 0; i < enemyList.size(); i++) {
			if (i >= 12) {
				break;
			}

			ghostTracker[i] = false;

			// get and set the position
			sf::Vector2f pos = sf::Vector2f(0, 0);
			if (enemyList[i].hasKey("position"))
			{
				json::JSON posJSON = enemyList[i]["position"];
				if (posJSON.hasKey("x"))
				{
					pos.x = posJSON["x"].ToFloat();
				}
				if (posJSON.hasKey("y"))
				{
					pos.y = posJSON["y"].ToFloat();
				}
			}
			ghostPool[i].SetPosition(pos);

			// Add components
			ghostPool[i].AddComponentsJSON(enemyList[i]);

			// Assign factory to ghost component
			((Ghost*)(ghostPool[i].GetComponent("Ghost")))->AssignFactory(this, i);
		}
	}
}

json::JSON Factory::Save()
{
	json::JSON document;
	document["className"] = "Factory";
	document["uid"] = this->GetGUID();
	document["spawnTimer"] = spawnTimer;
	document["spawnRate"] = spawnRate;
	document["dead"] = dead;

	document["enemies"] = json::JSON::Array();
	int size = sizeof(ghostTracker) / sizeof(bool);
	for (int i = 0; i < size; i++) {
		if (!ghostTracker[i]) { // false => active ghost
			document["enemies"].append(ghostPool[i].Save());
		}
	}

	return document;
}

void Factory::Die()
{
	// We have become deceased
	dead = true;

	// Unattach from tile
	Tile* t = gameManager->GetTile(game_object->GetPosition());
	t->UnattachObject();

	// Remove sprite component
	game_object->RemoveComponentByName("Sprite");
}

void Factory::FreeEnemy(int id)
{
	if (id >= 12 || id < 0) {
		return;
	}

	ghostTracker[id] = true;
}

void Factory::Update() {
	Enemy* enemy = (Enemy*)game_object->GetComponent("Enemy");
	if (enemy == nullptr) {
		return;
	}

	if (enemy->isActive()) {
		// Activate all active ghosts
		int size = sizeof(ghostTracker) / sizeof(bool);
		for (int i = 0; i < size; i++) {
			if (!ghostTracker[i]) { // false => ghost active
				((Enemy*)ghostPool[i].GetComponent("Enemy"))->Activate();
			}
		}

		if (!dead && enemy->GetHealth() <= 0) {
			Die();
			// Deactivate
			enemy->Deactivate();
			return;
		}

		// Should spawn something?
		if (!dead && spawnTimer >= spawnRate) {
			// TODO: implement enemy pool
			if (SpawnEnemy()) {
				spawnTimer = 0;
			}
		}
		else {
			spawnTimer++;
		}
		
		// Deactivate after turn
		enemy->Deactivate();
	}

	// Update all active ghosts
	int size = sizeof(ghostTracker) / sizeof(bool);
	for (int i = 0; i < size; i++) {
		if (!ghostTracker[i]) { // false => ghost active
			ghostPool[i].Update();
		}
	}
}

bool Factory::SpawnEnemy() {

	sf::Vector2f pos = game_object->GetPosition();
	Tile* curTile = gameManager->GetTile(pos);

	// First check for walls
	// Left
	if (!curTile->GetWall(3)) {
		if (TryToSpawnEnemy(pos + sf::Vector2f(-1.0f, 0))) {
			return true;
		}
	}

	// Right
	if (!curTile->GetWall(2)) {
		if (TryToSpawnEnemy(pos + sf::Vector2f(1.0f, 0))) {
			return true;
		}
	}

	// Up
	if (!curTile->GetWall(0)) {
		if (TryToSpawnEnemy(pos + sf::Vector2f(0, -1.0f))) {
			return true;
		}
	}

	// Down
	if (!curTile->GetWall(1)) {
		if (TryToSpawnEnemy(pos + sf::Vector2f(0, 1.0f))) {
			return true;
		}
	}

	return false;
}

bool Factory::TryToSpawnEnemy(sf::Vector2f spawnPos) {
	Tile* moveTile = gameManager->GetTile(spawnPos);
	if (!moveTile->isOccupied()) {
		json::JSON ghostJ = GhostData();
		GameObject* ghost = nullptr;
//		GameObject* ghost = GameObjectManager::Instance().LoadGameObject(ghostJ);

		// get array size
		int size = sizeof(ghostTracker) / sizeof(bool);
		// find first unused enemy
		int id = 0;
		for (int i = 0; i < size; i++) {
			if (ghostTracker[i]) {
				ghostTracker[i] = false;
				ghost = &(ghostPool[i]);
				id = i;
				break;
			}
		}
		// Did we find a free ghost?
		if (ghost == nullptr) {
			return false;
		}

		// Assign game object values
		ghost->SetPosition(spawnPos);

		// Add components
		ghost->AddComponentsJSON(ghostJ);

		// Assign factory to ghost component
		((Ghost*)ghost->GetComponent("Ghost"))->AssignFactory(this, id);

		//moveTile->AttachObject(ghost);
		return true;
	}
	return false;
}

json::JSON Factory::GhostData()
{
	// Randomly spawn either ghost or poison ghost
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	int r = rand() % 100;
	std::string fileName;
	if (r <= 66) {
		fileName = "../Assets/Ghost.json";
	}
	else {
		fileName = "../Assets/PoisonGhost.json";
	}
	
	return FileSystem::Instance().Load(fileName);
}
