#pragma once
#ifndef _FACTORY_H_
#define _FACTORY_H_

#include "Component.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Ghost.h"
#include "Sprite.h"

class GameManager;

class Factory : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Factory, Component)

private:
	int spawnTimer = 0;
	int spawnRate = 5;
	GameObject ghostPool[12];
	bool ghostTracker[12];
	bool SpawnEnemy();
	bool TryToSpawnEnemy(sf::Vector2f spawnPos);
	json::JSON GhostData();
	bool dead = false;
	GameManager* gameManager;

	/// <summary>
	/// Handle when health goes to 0
	/// </summary>
	void Die();

public:
	void FreeEnemy(int id);

protected:
	void Update() override;
	void Initialize() override;

	/// <summary>
	/// Load factory data from json
	/// </summary>
	/// <param name="json_component">JSON containing factory data</param>
	void Load(json::JSON& json_component) override;

	/// <summary>
	/// Save Factory data to json
	/// </summary>
	/// <returns>JSON containing factory data</returns>
	json::JSON Save() override;
};

#endif // !_FACTORY_H_