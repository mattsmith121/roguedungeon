#include "GameCore.h"
#include "TileManager.h"
#include "TileData.h"
#include "Sprite.h"
#include "Tile.h"
#include "FileSystem.h"

void TileManager::LoadTileData()
{
	// Use filesystem to load in tile data file and create tile data
	json::JSON tileDataJson = FileSystem::Instance().Load("../Assets/TileData.json");

	// Allocate vector memeory equal to number of entries in json
	tileData = std::vector<TileData*>(tileDataJson.size());

	// Assign tile data from json to vector
	for (int i = 0; i < tileDataJson.size(); i++) {
		TileData* t = new TileData(tileDataJson[i]);
		tileData[i] = t;
	}
}

GameObject* TileManager::CreateTile(int id, sf::Vector2f position)
{
	TileData* data = nullptr;
	
	// Find matching tile data
	for (auto it : tileData) {
		if (it->GetID() == id) {
			data = it;
			break;
		}
	}
	
	// If no data found, set to missing tile
	if (data == nullptr) {
		data = new TileData("../assets/Textures/defaultTexture.png");
	}

	std::vector<std::string> components = { "Tile", "Sprite" };
	GameObject* go = GameObjectManager::Instance().CreateGameObject("Tile", components, position);
	// Set sprite data
	Sprite* s = (Sprite*)go->GetComponent(components[1]);
	s->SetTexture(data->GetTexture());
	s->SetScale(0.25f, 0.25f);
	// Set tile data
	Tile* t = (Tile*)go->GetComponent(components[0]);
	t->SetData(data->GetWall(0), data->GetWall(1), 
			data->GetWall(2), data->GetWall(3));

	// Initialzie gameobject as CreateGameObject comes uninitialized
	go->Initialize();

	return go;
}
