#pragma once
#ifndef _TILE_H_
#define _TILE_H_

#include "Component.h"

class Potion;

/// <summary>
/// A Tile makes up the dungeon tilemap
/// </summary>
class Tile : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Tile, Component)
	
	bool wallTop;
	bool wallBottom;
	bool wallRight;
	bool wallLeft;
	GameObject* attachedObject;
	Potion* attachedItem;

public:
	~Tile() override {};

	/// <summary>
	/// Load in Tile data from json
	/// </summary>
	/// <param name="json_component">Json object containg tile data</param>
	void Load(json::JSON& jsonComponent) override;

	/// <summary>
	/// Save tile data to JSON
	/// </summary>
	/// <returns>JSON containing tile data</returns>
	json::JSON Save() override;

	/// <summary>
	/// Set tile data
	/// </summary>
	/// <param name="wallTop"> Is the top side a wall?</param>
	/// <param name="wallBottom"> Is the bottom side a wall?</param>
	/// <param name="wallRight"> Is the right side a wall?</param>
	/// <param name="wallLeft"> Is the left side a wall?</param>
	void SetData(bool wallTop, bool wallBottom, bool wallRight, bool wallLeft);

	/// <summary>
	/// Is there a wall in the given direction
	/// </summary>
	/// <param name="direction">0 = top, 1 = bottom, 2 = right, 3 = left</param>
	/// <returns>True if wall, false otherwise</returns>
	bool GetWall(int direction);

	/// <summary>
	/// Unattach the attached game object
	/// </summary>
	void UnattachObject();

	/// <summary>
	/// Attach a game object to this tile
	/// </summary>
	/// <param name="gameObject">Game object to attach</param>
	void AttachObject(GameObject* gameObject);

	/// <summary>
	/// Attach a potion to this tile
	/// </summary>
	/// <param name="potion">The potion to attach</param>
	void AttachItem(Potion* potion);

	/// <summary>
	/// Unattach the potion from this tile
	/// </summary>
	void UnattachItem();

	/// <summary>
	/// Is there an item attached to this tile?
	/// </summary>
	/// <returns>True if there is an item attached, false otherwise</returns>
	bool hasItem();

	/// <summary>
	/// Get the item attached to this tile
	/// </summary>
	/// <returns>Pointer to item attached, nullptr if no item attached</returns>
	Potion* GetItem();

	/// <summary>
	/// Is the tile occupied by another game object?
	/// </summary>
	/// <returns>True if the tile already has a gameobject attached to it.</returns>
	bool isOccupied();

	/// <summary>
	/// Get the object attached to this tile
	/// </summary>
	/// <returns>Pointer to attached object, nullptr if no object attached</returns>
	GameObject* GetAttachedObject();
};

#endif // !_TILE_H_