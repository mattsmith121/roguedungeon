#pragma once
#ifndef _PAUSEOBJECT_H_
#define _PAUSEOBJECT_H_

#include "Component.h"

class PauseObject : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(PauseObject, Component)

public:
	json::JSON Save() override;
};

#endif // !_PAUSEOBJECT_H_