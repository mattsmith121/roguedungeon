#pragma once
#ifndef _GHOST_H_
#define _GHOST_H_

#include "Component.h"

class Tile;
class GameManager;
class Factory;

class Ghost : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Ghost, Component)

private:
	GameObject* player;

	/// <summary>
	/// Determine if the ghost will move or attack
	/// </summary>
	/// <param name="pos">Position of ghost</param>
	/// <param name="playerPos">Position of player</param>
	void MoveOrAttack(sf::Vector2f pos, sf::Vector2f playerPos);

	/// <summary>
	/// Attack the player
	/// </summary>
	void Attack();

	/// <summary>
	/// Move the ghost to a new tile
	/// </summary>
	/// <param name="movePos">Position to move to</param>
	/// <param name="curTile">Current tile the ghost is on</param>
	/// <returns>True if the ghost was able to move to the new tile, false otherwise</returns>
	bool MoveToTile(sf::Vector2f movePos, Tile* curTile);

	/// <summary>
	/// Health is 0, clean things up and disable
	/// </summary>
	void Die();

	GameManager* gameManager;
	Factory* factory;
	int factoryID;
	int attackDamage;
	bool poison = false;
	bool dead;

protected:
	void Initialize() override;
	void Update() override;

	/// <summary>
	/// Load Ghost data from json
	/// </summary>
	/// <param name="jsonComponent">JSON containing ghost data</param>
	void Load(json::JSON& jsonComponent) override;

	json::JSON Save() override;

public:
	/// <summary>
	/// Assign a factory to this ghost
	/// </summary>
	/// <param name="factory">Factory to link to ghost</param>
	void AssignFactory(Factory* factory, int id);
};

#endif // !_GHOST_H_