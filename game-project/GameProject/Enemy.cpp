#include "GameCore.h"
#include "Enemy.h"
#include "GameManager.h"
#include "Tile.h"

IMPLEMENT_DYNAMIC_CLASS(Enemy)

void Enemy::Deactivate()
{
	activated = false;
}

bool Enemy::isActive()
{
	return activated;
}

void Enemy::TakeDamage(int damage)
{
	health -= damage;
}

int Enemy::GetHealth()
{
	return health;
}

void Enemy::Initialize()
{
	// start de-activated
	activated = false;
}

void Enemy::Load(json::JSON& jsonComponent)
{
	Component::Load(jsonComponent);
	ASSERT(jsonComponent.hasKey("health"), "Enemy missing 'health'");
	health = jsonComponent["health"].ToInt();
}

json::JSON Enemy::Save()
{
	json::JSON document;
	document["className"] = "Enemy";
	document["uid"] = this->GetGUID();

	document["health"] = health;

	return document;
}

void Enemy::Activate() {
	activated = true;
}
