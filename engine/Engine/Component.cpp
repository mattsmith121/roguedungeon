///-------------------------------------------------------------------------------------------------
// file: Component.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "Component.h"

IMPLEMENT_ABSTRACT_CLASS(Component)

Component::~Component()
{
}

GameObject* Component::GetGameObject()
{
	return game_object;
}

void Component::Initialize()
{
	Object::Initialize();
}

void Component::Load(json::JSON& jsonComponent)
{
	Object::Load(jsonComponent);
}

void Component::Update()
{
}
