///-------------------------------------------------------------------------------------------------
// file: RenderSystem.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "RenderSystem.h"
#include "IRenderable.h"

IMPLEMENT_SINGLETON(RenderSystem)

void RenderSystem::CloseWindow()
{
	window->close();
	delete window;
	window = nullptr;
}

void RenderSystem::Initialize()
{
	std::cout << "RenderSystem initialized" << std::endl;

	window = new sf::RenderWindow(sf::VideoMode(width, height), name);
	window->setFramerateLimit(60);
}

void RenderSystem::Load(json::JSON& document)
{
	if (document.hasKey("RenderSystem"))
	{
		json::JSON render_system = document["RenderSystem"];

		if (render_system.hasKey("Name"))
		{
			name = render_system["Name"].ToString();
		}
		if (render_system.hasKey("width"))
		{
			width = render_system["width"].ToInt();
		}
		if (render_system.hasKey("height"))
		{
			height = render_system["height"].ToInt();
		}
		if (render_system.hasKey("fullscreen"))
		{
			fullscreen = render_system["fullscreen"].ToBool();
		}
	}
}

void RenderSystem::Update()
{
	if (window != nullptr)
	{
		window->clear();

		for (auto renderable : renderables)
		{
			renderable->Render();
		}

		window->display();
	}
}

void RenderSystem::AddRenderable(IRenderable* renderable)
{
	renderables.push_back(renderable);
}

void RenderSystem::RemoveRenderable(IRenderable* renderable)
{
	renderables.remove(renderable);
}

void RenderSystem::ClearRenderables()
{
	renderables.clear();
}
