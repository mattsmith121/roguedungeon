///-------------------------------------------------------------------------------------------------
// file: GameObject.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <list>
#include "json.hpp"
#include "Object.h"

#include "SFML/System.hpp"

class Component;

/// <summary>
/// Defines the GameObject class
/// </summary>
class GameObject final : public Object
{
	DECLARE_DYNAMIC_DERIVED_CLASS(GameObject, Object)

private:
	std::string name = "";
	std::list<Component*> components;
	std::list<Component*> components_to_remove;

	// This should be in a transform, and all game objects should have a transform
	//Transform *transform;
	sf::Vector2f position;

public:
	/// <summary>
	/// Deletes all components in the game object
	/// </summary>
	~GameObject();

	/// <summary>
	/// 
	/// </summary>
	/// <param name="json_game_object"></param>
	void Load(json::JSON& json_game_object) override;

	/// <summary>
	/// Save the gameobject to a json object
	/// </summary>
	/// <returns>JSON representing the gameobject state</returns>
	json::JSON Save();

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component_list"></param>
	void AddComponents(const std::vector<std::string>& component_list);

	/// <summary>
	/// Add components to gameobject through json list
	/// </summary>
	/// <param name="jsonList">List of json components</param>
	void AddComponentsJSON(json::JSON jsonList);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component"></param>
	void AddComponent(Component* component);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component"></param>
	void RemoveComponent(Component* component);

	/// <summary>
	/// Remove first component that matches component name
	/// </summary>
	/// <param name="component">Name of component to remove</param>
	void RemoveComponentByName(std::string component);

	/// <summary>
	/// Remove all components from gameobject
	/// </summary>
	void RemoveAllComponents();

	/// <summary>
	/// Get a component from the GameObject
	/// </summary>
	/// <param name="component">Name of component</param>
	/// <returns>Component if GameObject has it, otherwise nullptr</returns>
	Component* GetComponent(std::string component);

	/// <summary>
	/// Initialize the GameObject
	/// </summary>
	void Initialize() override;

	inline const std::string& getName() { return name; }
	inline void SetPosition(const sf::Vector2f& value) { position = value; }
	inline const sf::Vector2f& GetTilePosition() { return position * 64.0f; }
	inline const sf::Vector2f& GetPosition() { return position; }

private:
	/// <summary>
	/// 
	/// </summary>
	void Update();

	friend class GameObjectManager;
	friend class Factory;
};

#endif //GAMEOBJECT_H

