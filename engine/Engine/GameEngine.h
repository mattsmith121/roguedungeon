///-------------------------------------------------------------------------------------------------
// file: GameEngine.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEENGINE_H
#define GAMEENGINE_H

/// <summary>
/// Defines the GameEngine class
/// </summary>
class GameEngine final
{
public:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	/// <summary>
	/// Load a new scene, clearing the old scene.
	/// </summary>
	void LoadNewScene(std::string newScene);

	/// <summary>
	/// Load new scene from json
	/// </summary>
	/// <param name="newScene">JSON containing scene data</param>
	void LoadNewSceneJSON(json::JSON newScene);

	DECLARE_SINGLETON(GameEngine)
};

#endif //GAMEENGINE_H
