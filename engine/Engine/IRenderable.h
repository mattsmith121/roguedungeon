///-------------------------------------------------------------------------------------------------
// file: IRenderable.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef IRENDERABLE_H
#define IRENDERABLE_H

/// <summary>
/// Renderable aspect
/// </summary>
class IRenderable
{
protected:
	/// <summary>
	/// 
	/// </summary>
	IRenderable();

	/// <summary>
	/// 
	/// </summary>
	~IRenderable();

	/// <summary>
	/// Render method, must override to draw
	/// </summary>
	virtual void Render() = 0;

	friend class RenderSystem;
};

#endif // !IRENDERABLE_H
