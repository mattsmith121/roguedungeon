///-------------------------------------------------------------------------------------------------
// file: Sprite.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "Sprite.h"
#include "RenderSystem.h"
#include "GameObject.h"
#include "AssetManager.h"

IMPLEMENT_DYNAMIC_CLASS(Sprite)

void Sprite::Load(json::JSON& json_component)
{
	Component::Load(json_component);

	// Should be part of asset manager, we should look up the asset by a GUID
	ASSERT(json_component.hasKey("texture_filename"), "Json node missing");
	texture_filename = json_component["texture_filename"].ToString();

	// Get scale (optional)
	if (json_component.hasKey("scale")) {
		json::JSON jScale = json_component["scale"];
		if (jScale.hasKey("x")) {
			scaleX = jScale["x"].ToFloat();
		}
		else {
			scaleX = 1.0f;
		}
		if (jScale.hasKey("y")) {
			scaleY = jScale["y"].ToFloat();
		}
		else {
			scaleY = 1.0f;
		}
	}
	else {
		scaleX = scaleY = 1.0f;
	}

	// Get origin (optional)
	if (json_component.hasKey("originCenter")) {
		originCenter = json_component["originCenter"].ToBool();
	}

	// Get center of tile (optional)
	if (json_component.hasKey("centerOfTile")) {
		centerOfTile = json_component["centerOfTile"].ToBool();
	}

	// Get use tile coordinates (optional)
	if (json_component.hasKey("useTileCoordinates")) {
		useTileCoordinates = json_component["useTileCoordinates"].ToBool();
	}

	// Get color (optional)
	if (json_component.hasKey("color")) {
		json::JSON jColor = json_component["color"];
		int r = 0, g = 0, b = 0;
		if (jColor.hasKey("r")) {
			r = jColor["r"].ToInt();
		}
		if (jColor.hasKey("b")) {
			b = jColor["b"].ToInt();
		}
		if (jColor.hasKey("g")) {
			g = jColor["g"].ToInt();
		}
		color = sf::Color(r, g, b);
	}
}

json::JSON Sprite::Save() {
	json::JSON document;
	document["className"] = "Sprite";
	document["uid"] = this->GetGUID();

	document["texture_filename"] = texture_filename;

	json::JSON scaleJSON;
	scaleJSON["x"] = scaleX;
	scaleJSON["y"] = scaleY;
	document["scale"] = scaleJSON;

	document["originCenter"] = originCenter;
	document["centerOfTile"] = centerOfTile;
	document["useTileCoordinates"] = useTileCoordinates;

	json::JSON colorJSON;
	colorJSON["r"] = color.r;
	colorJSON["g"] = color.g;
	colorJSON["b"] = color.b;
	document["color"] = colorJSON;

	return document;
}

void Sprite::Initialize()
{
	AssignTexture();
}

void Sprite::Render()
{
	sf::Vector2f base = useTileCoordinates ? game_object->GetTilePosition() : game_object->GetPosition();
	if (centerOfTile) {
		sf::Vector2f offset = base + sf::Vector2f(32.0f, 32.0f);
		sprite.setPosition(offset);
	} else {
		sprite.setPosition(base);
	}
	sprite.setColor(color);
	RenderSystem::Instance().GetWindow().draw(sprite);
}

void Sprite::SetTexture(std::string& textureFilename) {
	texture_filename = textureFilename;

	AssignTexture();
}

void Sprite::SetScale(float x, float y)
{
	scaleX = x;
	scaleY = y;
}

void Sprite::SetColor(sf::Color color)
{
	this->color = color;
}

void Sprite::UseTileCoordinates(bool useTileCoordinates)
{
	this->useTileCoordinates = useTileCoordinates;
}

void Sprite::AssignTexture() {
	// This should look up an asset from the asset manager
	texture = AssetManager::Instance().GetTexture(texture_filename);

	sprite.setTexture(texture);
	sprite.setScale(scaleX, scaleY);
	if (originCenter) {
		sprite.setOrigin(texture.getSize().x * 0.5f, texture.getSize().y * 0.5f);
	}
}
