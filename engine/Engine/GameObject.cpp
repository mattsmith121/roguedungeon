///-------------------------------------------------------------------------------------------------
// file: GameObject.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameObject.h"
#include "Component.h"

IMPLEMENT_DYNAMIC_CLASS(GameObject)

GameObject::~GameObject()
{
	for (auto component : components)
	{
		delete component;
	}
	components.clear();
}

void GameObject::Initialize()
{
	for (auto component : components)
	{
		component->Initialize();
	}
}

void GameObject::Load(json::JSON& json_game_object)
{
	Object::Load(json_game_object);

	ASSERT(json_game_object.hasKey("name"), "GameObject missing 'name'");
	name = json_game_object["name"].ToString();

	if (json_game_object.hasKey("position"))
	{
		json::JSON position_node = json_game_object["position"];
		if (position_node.hasKey("x"))
		{
			position.x = position_node["x"].ToFloat();
		}
		if (position_node.hasKey("y"))
		{
			position.y = position_node["y"].ToFloat();
		}
	}

	if (json_game_object.hasKey("components"))
	{
		auto json_components = json_game_object["components"];
		for (auto& json_component : json_components.ArrayRange())	// Loop for components
		{
			ASSERT(json_component.hasKey("className"), "Component missing 'className'");
			std::string class_name = json_component["className"].ToString();

			Component* component = static_cast<Component*>(CreateObject(class_name.c_str()));
			component->Load(json_component);
			component->game_object = this;
			AddComponent(component);
		}
	}
}

json::JSON GameObject::Save()
{
	json::JSON document;
	document["name"] = name;
	document["uid"] = this->GetGUID();
	json::JSON posJSON;
	posJSON["x"] = position.x;
	posJSON["y"] = position.y;
	document["position"] = posJSON;
	document["components"] = json::JSON::Array();
	for (auto it : components) {
		document["components"].append(it->Save());
	}

	return document;
}

void GameObject::AddComponents(const std::vector<std::string>& component_list)
{
	for (std::string comp : component_list)
	{
		Component* component = (Component*)CreateObject(comp.c_str());
		component->game_object = this;
		AddComponent(component);
	}
}

void GameObject::AddComponentsJSON(json::JSON jsonList)
{
	if (!jsonList.hasKey("components")) {
		return;
	}

	json::JSON comp_objects = jsonList["components"];
	for (auto& comp : comp_objects.ArrayRange())
	{
		Component* component = (Component*)CreateObject(comp["className"].ToString().c_str());
		component->game_object = this;
		component->Load(comp);
		AddComponent(component);
		component->Initialize();
	}
}

void GameObject::Update()
{
	for (auto component : components)
	{
		component->Update();
	}

	for (auto component : components_to_remove)
	{
		components.remove(component);
		delete component;
	}
	components_to_remove.clear();
}

void GameObject::AddComponent(Component* component)
{
	components.push_back(component);
}

void GameObject::RemoveComponent(Component* component)
{
	components_to_remove.remove(component);
}

void GameObject::RemoveComponentByName(std::string component)
{
	for (auto it : components) {
		if (it->getDerivedClassName() == component) {
			components_to_remove.push_back(it);
			break;
		}
	}
}

void GameObject::RemoveAllComponents()
{
	for (auto it : components) {
		components_to_remove.push_back(it);
	}
}

Component* GameObject::GetComponent(std::string component) {
	for (auto it : components) {
		if ((*it).getDerivedClassName() == component) {
			return it;
		}
	}

	return nullptr;
}
