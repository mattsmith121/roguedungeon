///-------------------------------------------------------------------------------------------------
// file: GameObjectManager.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include <list>
#include "json.hpp"
#include <SFML/Graphics.hpp>

class GameObject;

/// <summary>
/// Defines the GameObjectManager class
/// </summary>
class GameObjectManager final
{
private:
	std::list<GameObject*> game_objects;
	std::list<GameObject*> game_objects_to_remove;

public:
	/// <summary>
	/// Loads all game objects avaialble in the document
	/// </summary>
	/// <param name="document"></param>
	void Load(json::JSON& document);

	/// <summary>
	/// Save the game state
	/// </summary>
	void Save();

	/// <summary>
	/// Load a single game object using json data
	/// </summary>
	/// <param name="document">JSON data of game object</param>
	GameObject* LoadGameObject(json::JSON& document);

	/// <summary>
	/// Add a GameObject
	/// </summary>
	void AddGameObject(GameObject* game_object);

	/// <summary>
	/// Tags a game object to be removed at the end of update
	/// </summary>
	/// <param name="game_object"></param>
	void RemoveGameObject(GameObject* game_object);

	/// <summary>
	/// Create an uninitialized game object with a list of components.
	/// </summary>
	/// <param name="component_list">Components to add to game object</param>
	/// <param name="position">Initial position of game object</param>
	/// <returns>An uniitialized game object</returns>
	GameObject* CreateGameObject(std::string name, const std::vector<std::string>& component_list, sf::Vector2f position);

	/// <summary>
	/// Finds the first instance of a game object with the given name
	/// </summary>
	/// <param name="gameObjectName">Name of the game object</param>
	/// <returns>Returns the first instance of a game object with the given name (nullptr if no object found)</returns>
	GameObject* GetGameObject(std::string gameObjectName);

	/// <summary>
	/// Finds all instances of game objects with given component
	/// </summary>
	/// <param name="componentName">Name of the component</param>
	/// <returns>List of game objects that have the given component</returns>
	std::list<GameObject*> GetGameObjectsWithComponent(std::string componentName);

	/// <summary>
	/// Remove all current game objects
	/// </summary>
	void ClearObjects();

private:
	/// <summary>
	/// Initialize the GameObjectManager
	/// </summary>
	/// <param name="document">The json document</param>
	void Initialize();

	/// <summary>
	/// Updates all game objects, as well this will destroy all game objects that have
	/// been tag for deletion after everything updates
	/// </summary>
	void Update();

public:
	static GameObjectManager& Instance()
	{
		if (_instance == nullptr)
		{
			_instance = new GameObjectManager();
		}
		return *_instance;
	}
private:
	static GameObjectManager* _instance;

	/// <summary>
	/// Destroy all game objects in the manager
	/// </summary>
	~GameObjectManager();

	GameObjectManager() = default;
	GameObjectManager(const GameObjectManager& other) = delete;
	GameObjectManager& operator= (const GameObjectManager& other) = delete;

	friend class GameEngine;
};

#endif //GAMEOBJECTMANAGER_H