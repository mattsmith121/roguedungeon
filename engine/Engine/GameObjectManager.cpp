///-------------------------------------------------------------------------------------------------
// file: GameObjectManager.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Component.h"
#include "FileSystem.h"

IMPLEMENT_SINGLETON(GameObjectManager)

GameObjectManager::~GameObjectManager()
{
	for (auto game_object : game_objects)
	{
		delete game_object;
	}
	game_objects.clear();
}

void GameObjectManager::Initialize()
{
	std::cout << "GameObjectManager initialized" << std::endl;
}

void GameObjectManager::Load(json::JSON& document)
{
	if (document.hasKey("GameObjects") == false) return;

	json::JSON json_game_objects = document["GameObjects"];
	for (auto& json_game_object : json_game_objects.ArrayRange())
	{
		GameObject* game_object = new GameObject();
		game_object->Load(json_game_object);
		AddGameObject(game_object);
		game_object->Initialize();
	}

	/// Post Init
	//for (auto game_object : game_objects)
	//{
	//	game_object->Start();
	//}
}

void GameObjectManager::Save()
{
	json::JSON document;
	document["GameObjects"] = json::JSON::Array();

	for (auto it : game_objects) {
		// Don't save tiles or pause objects
		if (it->GetComponent("Tile") != nullptr || 
			it->GetComponent("PauseObject") != nullptr) {
			continue;
		}
		document["GameObjects"].append(it->Save());
	}

	FileSystem::Instance().Save(document, "../Assets/Saves/SaveFile.json");
}

GameObject* GameObjectManager::LoadGameObject(json::JSON& document) {
	GameObject* game_object = new GameObject();
	game_object->Load(document);
	game_object->Initialize();
	AddGameObject(game_object);
	return game_object;
}

GameObject* GameObjectManager::CreateGameObject(std::string name, const std::vector<std::string>& component_list, sf::Vector2f position)
{
	// Create game object
	GameObject* go = new GameObject();

	// Set name
	go->name = name;

	// Set position
	go->SetPosition(position);

	// Add components
	go->AddComponents(component_list);
	
	// Add to list
	AddGameObject(go);

	return go;
}

GameObject* GameObjectManager::GetGameObject(std::string gameObjectName)
{
	for (auto it : game_objects) {
		if (it->getName() == gameObjectName) {
			return it;
		}
	}

	return nullptr;
}

std::list<GameObject*> GameObjectManager::GetGameObjectsWithComponent(std::string componentName)
{
	std::list<GameObject*> objects = std::list<GameObject*>();

	for (auto it : game_objects) {
		if (it->GetComponent(componentName) != nullptr) {
			objects.push_back(it);
		}
	}

	return objects;
}

void GameObjectManager::Update()
{
	for (auto game_object : game_objects)
	{
		// Don't update if object is marked for deletion
		if (std::find(game_objects_to_remove.begin(),
			game_objects_to_remove.end(),
			game_object) == game_objects_to_remove.end()) {
			game_object->Update();
		}
	}

	for (auto game_object : game_objects_to_remove)
	{
		game_objects.remove(game_object);
		delete game_object;
	}
	game_objects_to_remove.clear();
}

void GameObjectManager::AddGameObject(GameObject* game_object)
{
	// There could be some code here
	game_objects.push_back(game_object);
}

void GameObjectManager::RemoveGameObject(GameObject* game_object)
{
	game_objects_to_remove.push_back(game_object);
}

void GameObjectManager::ClearObjects()
{
	for (auto game_object : game_objects)
	{
		game_objects_to_remove.push_back(game_object);
	}
}