///-------------------------------------------------------------------------------------------------
// file: IRenderable.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "IRenderable.h"
#include "RenderSystem.h"

IRenderable::IRenderable()
{
	RenderSystem::Instance().AddRenderable(this);
}

IRenderable::~IRenderable()
{
	RenderSystem::Instance().RemoveRenderable(this);
}
