///-------------------------------------------------------------------------------------------------
// file: FileSystem.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "FileSystem.h"

IMPLEMENT_SINGLETON(FileSystem)

void FileSystem::Initialize()
{
	std::cout << "File System initialized" << std::endl;
}

json::JSON FileSystem::Load(const std::string& file_name)
{
	std::ifstream fileStream(file_name);
	std::string fileString((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());

	ASSERT(fileString != "", "File not found: " + file_name);

	json::JSON fileDocument = json::JSON::Load(fileString);

	return fileDocument;
}

void FileSystem::Save(const json::JSON json, const std::string& file_name)
{
	//json::JSON document;
	//document["GameObjects"] = json::JSON::Array();

	//document["GameObjects"].append("Yay");

	//std::ofstream ostrm("../Assets/Save.json");
	//ostrm << document.dump() << std::endl;

	std::ofstream ostrm(file_name);
	ostrm << json.dump() << std::endl;
}

void FileSystem::Update()
{
}