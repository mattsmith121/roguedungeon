///-------------------------------------------------------------------------------------------------
// file: Object.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "Object.h"

IMPLEMENT_ABSTRACT_CLASS(Object)

Object::Object()
{
	UUID _uid;
	CreateUUID(&_uid);

	uid = GUIDToSTRCODE(_uid);
	guid = GUIDTostring(_uid);
}

Object::~Object()
{
}

void Object::Initialize()
{
	initialized = true;
}

void Object::Load(json::JSON& jsonComponent)
{
	ASSERT(jsonComponent.hasKey("uid"), "Object missing uid");
	uid = GetHashCode(jsonComponent["uid"].ToString().c_str());
}
