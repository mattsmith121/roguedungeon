#pragma once
#ifndef _TEXTSPRITE_H_
#define _TEXTSPRITE_H_

#include "Component.h"
#include "IRenderable.h"
#include <SFML/Graphics.hpp>

class TextSprite : public Component, IRenderable
{
	DECLARE_DYNAMIC_DERIVED_CLASS(TextSprite, Component)

private:
	sf::Text text;
	sf::Font baseFont;

protected:
	~TextSprite() override {}
	void Initialize() override;

	/// <summary>
	/// Load component from json
	/// </summary>
	/// <param name="jsonComponent">JSON detailing component</param>
	void Load(json::JSON& jsonComponent) override;
	
	/// <summary>
	/// Save the component to JSON
	/// </summary>
	/// <returns>JSON containing textsprite info</returns>
	json::JSON Save() override;
	
	void Render() override;

public:
	void SetText(std::string textString);
};

#endif // !_TEXTSPRITE_H_