///-------------------------------------------------------------------------------------------------
// file: FileSystem.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef FILESYSTEM_H
#define FILESYSTEM_H

/// <summary>
/// Defines the FileSystem class
/// </summary>
class FileSystem final
{
public:
	/// <summary>
	/// Load in a json file and return the json
	/// </summary>
	/// <param name="file_name">Path to the file</param>
	/// <returns>JSON from the file</returns>
	json::JSON Load(const std::string& file_name);

	/// <summary>
	/// Save json to a file.
	/// </summary>
	/// <param name="file_name">Path to the file to save to</param>
	/// <param name="json">JSON to save to the file</param>
	void Save(const json::JSON json, const std::string& file_name);

	DECLARE_SINGLETON(FileSystem)

private:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	friend class GameEngine;
};

#endif //FILESYSTEM_H
