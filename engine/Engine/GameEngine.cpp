///-------------------------------------------------------------------------------------------------
// file: GameEngine.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameEngine.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"

IMPLEMENT_SINGLETON(GameEngine)

extern void RegisterEngineClasses();

void GameEngine::Initialize()
{
	RegisterEngineClasses();

	FileSystem::Instance().Initialize();
	FileSystem::Instance().Save(NULL, "");
	AssetManager::Instance().Initialize();
	GameObjectManager::Instance().Initialize();

	json::JSON game_settings_document = FileSystem::Instance().Load("../Assets/GameSettings.json");

	RenderSystem::Instance().Load(game_settings_document);
	RenderSystem::Instance().Initialize();

	// Need this after rendersystem to access window
	InputManager::Instance().Initialize();

	ASSERT(game_settings_document.hasKey("GameEngine"), "GameEngine missing 'GameEngine' field");
	json::JSON game_engine = game_settings_document["GameEngine"];

	ASSERT(game_engine.hasKey("DefaultScene"), "GameEngine missing default scene.");
	std::string default_file = game_engine["DefaultScene"].ToString();

	json::JSON default_file_document = FileSystem::Instance().Load(default_file.c_str());

	GameObjectManager::Instance().Load(default_file_document);
}

void GameEngine::Update()
{
	std::chrono::time_point<std::chrono::system_clock> time;
	std::chrono::duration<float> deltaTime(0);
	std::chrono::duration<float> totalTime(0);

	while (RenderSystem::Instance().HasRenderWindow())
	{
		time = std::chrono::system_clock::now();

		FileSystem::Instance().Update();
		AssetManager::Instance().Update();

		InputManager::Instance().Update();

		GameObjectManager::Instance().Update();

		RenderSystem::Instance().Update();

		deltaTime = std::chrono::system_clock::now() - time;
		totalTime += deltaTime;
	}
}

void GameEngine::LoadNewScene(std::string newScene)
{
	// Blow away old scene
	GameObjectManager::Instance().ClearObjects();
	AssetManager::Instance().RemoveAssets();
	RenderSystem::Instance().ClearRenderables();

	// Load new scene
	json::JSON newSceneDocument = FileSystem::Instance().Load(newScene);

	GameObjectManager::Instance().Load(newSceneDocument);
}

void GameEngine::LoadNewSceneJSON(json::JSON newScene)
{
	// Blow away old scene
	GameObjectManager::Instance().ClearObjects();
	AssetManager::Instance().RemoveAssets();
	RenderSystem::Instance().ClearRenderables();

	GameObjectManager::Instance().Load(newScene);
}
