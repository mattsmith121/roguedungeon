///-------------------------------------------------------------------------------------------------
// file: AssetManager.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "AssetManager.h"
#include "Asset.h"

IMPLEMENT_SINGLETON(AssetManager)

void AssetManager::RemoveAssets()
{
	// Clear all textures from the map
	textures.clear();
}

sf::Texture AssetManager::GetTexture(std::string texturePath)
{
	// See if texture exists in map
	if (textures.find(texturePath) == textures.end()) {
		// Texture not yet in map, load and add it
		sf::Texture t;
		if (t.loadFromFile(texturePath)) {
			textures.emplace(texturePath, t);
		}
		else {
			// Texture didn't load, use default texture
			textures.emplace(texturePath, defaultTexture);
		}
	}

	return textures[texturePath];
}

void AssetManager::Initialize()
{
	// load default texture
	defaultTexture.loadFromFile("../Assets/Textures/defaultTexture.png");

	std::cout << "Asset Manager initialized" << std::endl;
}

void AssetManager::Update()
{
}
