#include "Core.h"
#include "TextSprite.h"
#include "RenderSystem.h"
#include "GameObject.h"

IMPLEMENT_DYNAMIC_CLASS(TextSprite);

void TextSprite::Initialize()
{
	// TODO: do this in assetmanager
	if (!baseFont.loadFromFile("../Assets/Font/SansitaSwashed-Regular.ttf")) {
		// TODO: handle missing font file
		return;
	}
}

void TextSprite::Load(json::JSON& jsonComponent)
{
	Component::Load(jsonComponent);

	// Get the text
	ASSERT(jsonComponent.hasKey("text"), "TextSprite missing 'text'");
	text.setString(jsonComponent["text"].ToString());
	
	// Get font size (optional)
	if (jsonComponent.hasKey("fontSize")) {
		text.setCharacterSize(jsonComponent["fontSize"].ToInt());
	}
	else {
		text.setCharacterSize(16);
	}

	// Get color (optional)
	if (jsonComponent.hasKey("fontColor")) {
		json::JSON jColor = jsonComponent["fontColor"];
		int r = 0, g = 0, b = 0;
		if (jColor.hasKey("r")) {
			r = jColor["r"].ToInt();
		}
		if (jColor.hasKey("b")) {
			b = jColor["b"].ToInt();
		}
		if (jColor.hasKey("g")) {
			g = jColor["g"].ToInt();
		}
		text.setFillColor(sf::Color(r, g, b));
	}


	// Only have one font
	text.setFont(baseFont);
}

json::JSON TextSprite::Save()
{
	json::JSON document;
	document["className"] = "TextSprite";
	document["uid"] = this->GetGUID();

	document["text"] = text.getString();
	document["fontSize"] = text.getCharacterSize();

	json::JSON colorJSON;
	colorJSON["r"] = text.getFillColor().r;
	colorJSON["g"] = text.getFillColor().g;
	colorJSON["b"] = text.getFillColor().b;
	document["fontColor"] = colorJSON;

	return document;

}

void TextSprite::Render() {
	// Draw the text
	text.setPosition(game_object->GetPosition());
	RenderSystem::Instance().GetWindow().draw(text);
}

void TextSprite::SetText(std::string textString)
{
	text.setString(textString);
}
