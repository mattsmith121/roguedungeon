///-------------------------------------------------------------------------------------------------
// file: Sprite.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef SPRITE_H
#define SPRITE_H

#include "Component.h"
#include "IRenderable.h"
#include "SFML/Graphics.hpp"

/// <summary>
/// Draws a sprite to the SFML window
/// </summary>
class Sprite : public Component, IRenderable
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Sprite, Component)

protected:
	~Sprite() override {}

	/// <summary>
	/// Initialize the Sprite
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// Load the sprite data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;

	/// <summary>
	/// Save the sprite to json
	/// </summary>
	/// <returns>JSON containing the sprite information</returns>
	json::JSON Save() override;

	/// <summary>
	/// Render the Sprite
	/// </summary>
	void Render() override;

private:
	std::string texture_filename;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::IntRect spriteRect;
	float scaleX = 1.0f;
	float scaleY = 1.0f;
	bool originCenter = false; // use center of sprite as origin
	bool centerOfTile = false; // draw in center of tile, instead of top right
	bool useTileCoordinates = true; // use adjusted tile coordinates for drawing
	sf::Color color = sf::Color::White;

	/// <summary>
	/// Assign texture to SFML object and set properties
	/// </summary>
	void AssignTexture();

public:

	/// <summary>
	/// Set the texture of the sprite
	/// </summary>
	/// <param name="texture">The path to the texture</param>
	void SetTexture(std::string& texture);

	/// <summary>
	/// Set the scale of the sprite
	/// </summary>
	/// <param name="x">X scale</param>
	/// <param name="y">Y scale</param>
	void SetScale(float x, float y);

	/// <summary>
	/// Set the color of the sprite
	/// </summary>
	/// <param name="color">The color to set the sprite to</param>
	void SetColor(sf::Color color);

	/// <summary>
	/// Sets whether the sprite should use tile coordinates to draw or just regular coordinates
	/// </summary>
	/// <param name="useTileCoordinates">True if use tile coordinates, false for regular coordinates</param>
	void UseTileCoordinates(bool useTileCoordinates);
};

#endif // !SPRITE_H
