///-------------------------------------------------------------------------------------------------
// file: AssetManger.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include "SFML/Graphics.hpp"

/// <summary>
/// Defines the AssetManager class
/// </summary>
class AssetManager final
{
private:
	std::map<std::string, sf::Texture> textures;
	sf::Texture defaultTexture;

public:
	/// <summary>
	/// Clear all Textures
	/// </summary>
	/// <param name="asset"></param>
	void RemoveAssets();

	/// <summary>
	/// Get a texture
	/// </summary>
	/// <param name="texturePath">The path to the texture</param>
	/// <returns>The texture specified, default texture if no texture found.</returns>
	sf::Texture GetTexture(std::string texturePath);

private:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	DECLARE_SINGLETON(AssetManager)

	friend class GameEngine;
};

#endif //ASSETMANAGER_H
