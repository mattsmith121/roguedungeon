///-------------------------------------------------------------------------------------------------
// file: Component.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef COMPONENT_H
#define COMPONENT_H

#include "Object.h"

class GameObject;

/// <summary>
/// Defines the Component class
/// </summary>
class Component : public Object
{
	DECLARE_ABSTRACT_DERIVED_CLASS(Component, Object)

protected:
	GameObject* game_object;

public:
	~Component() = 0;

	/// <summary>
	/// Get the game object this component is attached to
	/// </summary>
	/// <returns>Pointer to game object of component</returns>
	GameObject* GetGameObject();

protected:
	/// <summary>
	/// Initialize the component
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// Load component data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& jsonComponent) override;

	/// <summary>
	/// Save the component to JSON
	/// </summary>
	/// <returns>JSON containing the state of the component</returns>
	virtual json::JSON Save() = 0;

	/// <summary>
	/// Updates the Component
	/// </summary>
	virtual void Update();

	friend class GameObject;
	friend class GameObjectManager;
};

#endif //COMPONENT_H